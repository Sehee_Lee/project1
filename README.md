# ** Flower Garden ** 


 


This game is to grow a flower garden.

You can customize characters, grow and cross-breed basic flower seeds to find new colored flowers and fill the collection.

All materials including code and video are here:
https://bitbucket.org/Sehee_Lee/project1/src/master/



### 1. Inspiration 

    I like "Stardew Balley" and the "Animal Forest" very much.
    So I wanted to make a game that heals in nature like those.

~~Also I haven't bought a Nintendo Switch yet...~~





### 2. Libraries I used 

I used these libraries:

    - controlP5
    - interfascia
    - processing.sound

controlP5 and interfascia are used for GUI, and sound is used for BGM and soundeffect.




### 3. sources

    I brought some part of the code from Homework 5.
    The soundeffects were musics for free by youtube audio library.
    The image datas used for this game are all drawn by myself.




### 4. code structure 

I made 6 class:

    - Field
    - Section
    - Flower
    - FlowerCollection
    - User
    - UI

and there are 4 important functions:

    - keyPressed() : for many thing done by keyboard input
    - mousePressed() : for many thing done by mouse input
    - GUI_setup() : setup Cp5 GUI
    - GUI_draw() : draw Cp5 GUI



#####      A.Field

`class Field` has a section array in the instance.
`class Field` has many functions to access the section and to set some variables of the section.
Except some simple functions to set variable, the core functions are these:

| Function name     | Description                    |
| ----------------- | ------------------------------ |
| `draw()`          | draw the field.                |
| `crossbreeding()`   | for every section in field, check if the section can make a new crossbreed and make that  |


#####      B. Section

`Class Section` has many variable to save data of a section.
All of the functions are for setting variable from outer parameter.
So I listed here only the variables.

| variable          | Description                    |
| ----------------- | ------------------------------ |
|  `int date`       | How long has it been since you planted it?|
| `boolean scrted`  | is the section plowed? |
| `boolean watered` |is the section watered?|
| `boolean seeded` | is the section seeded?|
|`boolean bloom`|is the flower bloomed?|
|`int c`            | the color of flower|
|`int kind`| the kind of flower|




##### C.FlowerCollection

`class FlowerCollection` is made for collection page. Its instance has a flower array like `class Field`.

| Function name     | Description                    |
| ----------------- | ------------------------------ |
| `draw()`          | draw the field.                |
| `crossbreeding()`   | for every section in field, check if the section can make a new crossbreed and make that  |




##### D. Flower

`class Flower` is made to save each data of flower and make it as a array to make a collection page.

| variable name     | Description                    |
| ----------------- | ------------------------------ |
| `int c`          | the color of flower             |
|`int kind` | the kind of flower|
|`String name`|the name of flower from c, kind|
|`boolean possible`|did user found the hidden color?|




##### E. User


`class User` is made to save user statement and draw the user's outfit.
Most of the functions are for set variable from outer parameter, so I'll introduce variables.

| variable name     | Description                    |
| ----------------- | ------------------------------ |
| `String name`   | name of the character   |
| `int state`   | the tool number character hold |
| `int date`   | date of user played|
|`color hair` | color of hair|
|`color skin`|color of skin|
|`int hairmode`|hairstyle of character|
|`int x` | X location of character|
|`int y`| Y location of character|
|`And many PImage for outfit...`|


##### F. UI

`class UI` if made for garden UI. It represents user's name, date, tool.
There is no distictive function and variabe except `draw.()`. I think it would've been okay if I just made this as a function, not a class. At first, I expected the class to be appropriate, so I made it like this.





### 5. How to Play


##### A. Customize your character

    In the first page, you can customize your character.
    If you are ready, click the 'START' button.


#####   B. Moving Character

    you can move the character with the keyboard 'UP' 'DOWN' 'LEFT' 'RIGHT'.


#####   C. Setting tools

    You can change the tool with the keyboard '1', '2', '3'.
    If you want to change the kind of seed, click the seed image.
    Then you can see the collections.
    you can choose the kind of seed by click the flower image.
    If you click the seed image one more time, the collection page is end.


#####   D. Using tools

    If you want to use the tool, use 'SPACEBAR' on the section.
    You have to plow first, then you can water of seed to the section.
    In addition, a seed or plant on the section can grow when the section is watered.
    If you want to reset the section, just plow the section again.


#####   E. Passing a day

    After you finish all things to do in the day, you can pass a day.
    Go to in front of the house, and press the '4' key.
    After night comes down, new day will start.


#####   F. Crossbreeding

    After bloom, you do not have to water the section.
    If the two adjacent areas are blooming, the new cross-breeding seeds spread out to other plowed setion.
    The crossbreeding results depend on the color of the two parent flowers.
    Find the hidden colors, and fill up the collections. Good Luck!


