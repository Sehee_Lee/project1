import processing.sound.*;

import interfascia.*;

/*
	Project 1
	Name of Project: Flower Farm
	Author: Sehee Lee
	Date: 2020-05
*/
Field field;
User user;
UI ui;
FlowerCollection FC;
PFont Chalkboard;

SoundFile BGM;
SoundFile watering;
SoundFile plowing;
SoundFile seeding;

//private RowCol flower_state = new RowCol(0,0);

int farm_x=200;
int farm_y=350;
boolean gui;
boolean collect=false;

boolean nit;

color bg;

color night;
float t;

PImage cloud;

float cloudX;

  PImage hair_img ;
  PImage skin_img;
  PImage cloth_img ;
  PImage hat_img ;
  PImage eye_img;
  PImage medium_img ;
  PImage long_img;
  
  PImage grass;

void setup()
{
	// your code

  

  gui = true;
  size(900,900);
  
  
  field = new Field(500);
  user = new User("sehee");
  ui = new UI();
  FC = new FlowerCollection();
  cloud = loadImage("cloud.png");
  
  t=0;
  nit = false;
  
  hair_img = loadImage("hair.png");
  skin_img = loadImage("skin.png");
  cloth_img = loadImage("cloth.png");
  hat_img = loadImage("hat.png");
  eye_img = loadImage("eye.png");
  medium_img = loadImage("short.png");
  long_img = loadImage("long.png");
  grass = loadImage("grass.png");
  
  
  bg = color(180,235,255);
  night = color(180,235,255);

  cloudX =50;
  
  GUI_setup();
  
  rectMode(CORNER);
  
  BGM = new SoundFile(this, "BGM.mp3");
  watering = new SoundFile(this,"watering.mp3");
  plowing = new SoundFile(this,"plowing.mp3");
  seeding = new SoundFile(this, "seeding.mp3");
  
  BGM.amp(0.2);
  BGM.loop();
}

void draw()
{
	// your code
  

  background(bg);
  
  

  
  stroke(40,150,20);
  fill(58,170,53);
  rect(0,230,1000,800);
  
  image(cloud,cloudX,50,200,100);
  
  image(cloud,cloudX+400,40,200,100);
  
  image(cloud,cloudX-400,40,200,100);
  image(cloud,cloudX+-800,40,200,100);
  
  cloudX = cloudX+0.1;
  
  pushMatrix();
  translate(farm_x,farm_y);
  
  

  field.draw();
  
  float sSize = 500/7;
  
  
  RowCol userXY = field.coordToRowCol(user.getX()-farm_x, user.getY()-farm_y);
  if (user.getX()>farm_x && user.getY()>farm_y && user.getX()<farm_x+500  && user.getY()<farm_y+500){
    noStroke();
  fill(255,255,255,50);
  rect(userXY.row()*sSize, userXY.col()*sSize, sSize, sSize);
  }
  popMatrix();
  


  PImage house = loadImage("house.png");
  image(house, 400,10, 300,300);
  
  fill(0);
  textSize(15);
  text("4",565,240);
  
  user.draw();
  
  ui.draw(0,200,user);
  
  image(grass,100,400,100,100);
image(grass,50,700,100,100);
image(grass,800,500,100,100);
//image(grass,100,400,100,100);
  
  
  //println(user.getX());

  
  if (gui==true)
  {GUI_draw();}
  else
  {
    cp5.hide();
  }

  if (collect == true)
  {
  FC.draw(100,400);
  }

  noStroke();
  fill(0,0,0,t);
  rect(0,0,900,900);
  
    if (nit==true)
    t +=(240-t)*0.2;
   
    else if (nit==false){
    t -=(t-1)*0.1;
    }
       
    if (t>235) nit=false;
  

}


void mousePressed()
{
  //mouse xy to RowCol
  if (mouseX<100 && mouseY<500 && mouseY > 400)
  {
    if (collect==false)
    collect = true;
    else
    collect = false;
  }
  if (mouseX>100 && mouseX<740&& mouseY<640 && mouseY > 400)
  {
    if(collect == true)
    {

    pushMatrix();
    translate(FC.getX(), FC.getY());
    
    RowCol mouseXY = FC.coordToRowCol(mouseX-FC.getX(), mouseY-FC.getY());
    
    popMatrix();
    
    if (FC.getFlower(mouseXY.row(),mouseXY.col()).getPossible()==true)
    FC.setState(mouseXY);
    }
  }
    

}

void keyPressed()
{
  if (key == '1'){user.setState(1);}
  else if (key == '2'){user.setState(2);}
  else if (key == '3'){user.setState(3);}
  else if (key == '4')
  {
    if (user.getX()<700 && user.getX()>400 && user.getY()<350)
    {
    for (int row = 0; row<7; row++)
       for (int col = 0; col<7; col++)
       {
         if (field.getSection(row,col).getSeeded()== true && field.getSection(row,col).getScrted()== true&&field.getSection(row,col).getWatered()== true)
         {field.set_section_date(row,col);
          field.set_section_watered(row,col,false);
         }
         
         else if(field.getSection(row,col).getWatered() == true)
         {
           field.set_section_watered(row,col,false);
           
         }
       } 
    user.setDate();
    field.crossbreeding();
    nit = true;
    }
  }
  //else if (key == '5'){ ; gui = false; }
  

  int v = 9;
  
  if (keyCode==LEFT){user.setX(user.getX()-v); user.set_move("left");}
  if (keyCode == RIGHT){user.setX(user.getX()+v);user.set_move("right");}
  if (keyCode == UP)
  {
  
    if (user.getX()<700 && user.getX()>400)
    {
      if (user.getY()>310)
      
       user.setY(user.getY()-v);
    
    }
    else if (user.getY()>230) 
    {user.setY(user.getY()-v);}

    user.set_move("up");

   }
  if (keyCode == DOWN){user.setY(user.getY()+v);user.set_move("down");}
 
  if (key == ' ')
  {
   pushMatrix();
   translate(farm_x,farm_y);

   if (user.getX()>farm_x && user.getY()>farm_y)
     {
     RowCol userXY = field.coordToRowCol(user.getX()-farm_x, user.getY()-farm_y);
     
     if (user.getState() ==1)
     {
       if (field.getSection(userXY).getScrted()==true) 
       {
       field.set_section_scrted(userXY,false);
       field.set_section_seeded(userXY,false);
       field.set_section_bloom(userXY,false);
       field.set_section_date(userXY,0);
        plowing.play();
       
         if (field.getSection(userXY).getWatered()==true)
         {
         field.set_section_watered(userXY,false);
         }
       }
       else {field.set_section_scrted(userXY,true); plowing.play();}
     }
     
     
     else if (user.getState() == 2)//watermode
     {
       if(field.getSection(userXY).getScrted()==true)
       {
       
         if(field.getSection(userXY).getWatered()==false) {field.set_section_watered(userXY,true); watering.play();}
         //else {field.set_section_watered(mouseXY,true);}
       }
       else {
       int a=0;
       
         while(a<50)
         {
           fill(0);
           text("Plow first!", user.getX()-farm_x-30,user.getY()-120-farm_y);
           a=a+1;
         }
       
       }
       
     }
     else if (user.getState() == 3) //seedmode
     {
       if(field.getSection(userXY).getScrted()==true)
       {
       
         if(field.getSection(userXY).getSeeded()==false) 
         {
         field.set_section_seeded(userXY,true);
         field.set_flower(userXY, new Flower(FC.getState().row(), FC.getState().col()).get_name());
         field.set_flower_int(userXY,FC.getState().row(),FC.getState().col());
         
         seeding.play();
         }
         else {}
       }
       else{
       int a=0;
       
         while(a<50)
         {
           fill(0);
           text("Plow first!", user.getX()-farm_x-30,user.getY()-120-farm_y);
           a=a+1;
         }
       
       }
     }
   }
   popMatrix();
    
   
    
    
  }
  
  
}



// your code down here
// feel free to crate other .pde files to organize your code


public class RowCol
{
  private int row,col;
  
  public RowCol (int r, int c)
  {
    row=r; col=c;
  }
  
  public int row(){return row;}
  public int col(){return col;}
  public String toString(){return row+","+col;}
  
  
}
