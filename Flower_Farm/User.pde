public class User
{
  private String name;
  
  private int state;
  int date;
  
  color hair;
  color skin;
  int hairmode;
  
  private int x;
  private int y;
  
  String move;
  
  private PImage hair_img = loadImage("hair.png");
  private PImage skin_img = loadImage("skin.png");
  private PImage cloth_img = loadImage("cloth.png");
  private PImage hat_img = loadImage("hat.png");
  private PImage eye_img = loadImage("eye.png");
  private PImage medium_img = loadImage("short.png");
  private PImage long_img = loadImage("long.png");
  
  private PImage tool1 = loadImage("tool1.png");
  private PImage tool2 = loadImage("tool2.png");
  private PImage tool3 = loadImage("tool3.png");
  
  User(String name)
  {
    this.name = name;
    state=1;
    //loc = new PVector (10,10);
    date = 1;
    x=500;
    y=350;
    hair = color(0,0,0);
    skin = color(0,0,0);
    hairmode = 1;
    
    move = "down";

  }
  
  public void draw()
  {

    int char_size = 30;
    
    //shadow
    ellipseMode(CENTER);
    
    noStroke();
    fill(0,0,0,50);
    ellipse(x,y+8,40,10);
    
    
    imageMode(CENTER);
      
    if (hairmode==2){tint(hair);image(medium_img,x,y-50,char_size*3,char_size*4);}
    else if (hairmode==3){tint(hair); image(long_img,x,y-50,char_size*3,char_size*4);}
    
    tint(skin);
    image(skin_img,x,y-50,char_size*3,char_size*4);
    
    tint(hair);
    image(hair_img,x,y-50,char_size*3,char_size*4);
    
    noTint();
    
    image(cloth_img,x,y-50,char_size*3,char_size*4);
    image(hat_img,x,y-50,char_size*3,char_size*4);
    image(eye_img,x,y-50,char_size*3,char_size*4);
  
    
    if (state == 1){image(tool1,x+30,y-35,60,60);}
    else if (state == 2) {image(tool2,x+30,y-10,60,60);}
    else if (state==3){image(tool3,x+20,y-10,40,40);}

    imageMode(CORNER);
    

  }
  
  public void setState(int s){state = s;}
  public int getState(){return state;}
  
  
  public void set_name(String name){this.name = name;}
  public String getName(){return name;}
  
  
  public void setDate(){date++;}
  public int getDate(){return date;}
  
  
  public void setX(int x){this.x=x;}
  public void setY(int y){this.y=y;}
  
  
  public int getX(){return x;}
  public int getY(){return y;}
  
  
  public void set_hair(int r, int g, int b){hair = color(r,g,b);}
  public void set_skin(int r, int g, int b){skin = color(r,g,b);}
  public void set_hairmode(int h){hairmode = h;}
  
  public void set_move(String s){move = s;}
  
  
  //public void setFlower(RowCol rc){flower_state =rc;}
  //public RowCol getFlower() {return flower_state;}
}
