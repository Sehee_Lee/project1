
public class Section
{
  
  int state; 
  
  int date;
  
  PVector xyLoc;
  
  boolean scrted;
  boolean watered;
  boolean seeded;
  boolean bloom;
  float sz;
  
  String flower_name;
  int c;
  int kind;
  
  
  Section(PVector xy, float size)
  {
    
    date = 0;
    scrted = false;
    watered = false;
    seeded = false;
    bloom = false;
    xyLoc = xy;
    sz = size;
    flower_name = "";
    //scrt_img =loadImage("scrt.png");
    //seed_img =loadImage("seed.png");
    
  }
  
  public void draw()
  {
    
  }
  
  public void setScrted(boolean b) {scrted = b;}
  public void setSeeded(boolean b) {seeded = b;}
  public void setWatered(boolean b) {watered = b;}
  public void setBloom(boolean b) {bloom = b;}
  
  
  public void setFlowerInt(int c,int kind){this.c = c; this.kind = kind;}
  
  public boolean getScrted() {return scrted;}
  public boolean getSeeded(){return seeded;}
  public boolean getWatered(){return watered;}
  public boolean getBloom(){return bloom;}
  
  public int getC(){return c;}
  
  public int getKind(){return kind;}
  
  
  
  
  public String getFlower()
  {

    return flower_name;
  }

public void setFlower(String s)
{
  flower_name = s;
}
  
  public int getDate(){return date;}
  public void setDate(){date=date+1;}
  public void setDate(int date){this.date = date;}
  
}
