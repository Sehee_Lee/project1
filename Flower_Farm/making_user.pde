
import controlP5.*;

ControlP5 cp5;
int myColor = color(0,0,0);

int r = 100;
int g = 100;
int b = 100;

  int hairmode = 1;

float n;

int skin = 100;
int sliderTicks1 = 100;
int sliderTicks2 = 30;
Slider abc;

String textValue = "user";


RadioButton r1, r2;

void GUI_setup() {

  
 noStroke();
 cp5 = new ControlP5(this);
 
 
  r1 = cp5.addRadioButton("radioButton")
 .setPosition(600,480)
 .setSize(60,30)
 .setColorForeground(color(120))
 .setColorBackground(color(67,41,24))
 .setColorActive(color(120,90,70))
 //.setColorLabel(color(249,184,117))
 .hideLabels()
 .setItemsPerRow(3)
 .setSpacingColumn(10)
 .addItem(" short",1)
 .addItem("medium",2)
 .addItem("  long",3)
 ;

  
  
   for(Toggle t:r1.getItems()) 
   {
   //t.getCaptionLabel().setColorBackground(color(67,41,24));
   t.getCaptionLabel().getStyle().moveMargin(-7,0,0,-60);
   t.getCaptionLabel().getStyle().movePadding(7,0,0,3);
   t.setFont(createFont("Chalkboard",12));
   //t.getCaptionLabel().getStyle().backgroundWidth = 45;
   //t.getCaptionLabel().getStyle().backgroundHeight = 13;
   }
   
  
  
  
 cp5.addSlider("r")
 .setPosition(600,370)
 .setRange(0,255)
 .setSize(200,20)
 .setColorForeground(color(255,0,0))
 .setColorBackground(color(67,41,24))
 .setColorActive(color(255,80,80))
 .setFont(createFont("Chalkboard",15))
 ;
 
 cp5.addSlider("g")
 .setPosition(600,400)
 .setRange(0,255)
 .setSize(200,20)
 .setColorForeground(color(0,255,0))
 .setColorBackground(color(67,41,24))
 .setColorActive(color(80,255,80))
 .setFont(createFont("Chalkboard",15))
 ;
  
 cp5.addSlider("b")
 .setPosition(600,430)
 .setRange(0,255) 
 .setSize(200,20)
 .setColorForeground(color(0,0,255))
 .setColorBackground(color(67,41,24))
 .setColorActive(color(50,50,255))
 .setFont(createFont("Chalkboard",15))
 ;
 
 
 
 cp5.addSlider("skin")
 .setPosition(600,570)
 .setRange(0,180)
 .setSize(200,20)
 .setColorBackground(color(255,255,255))
 .setColorForeground(color(67,41,24))
 .setColorValue(color(0,0,0,0))
 .setLabelVisible(false)
 .setColorActive(color(102,66,46))
 .setFont(createFont("Chalkboard",40))
 ;
 
 
  cp5.addTextfield("NAME")
 .setPosition(600,270)
 .setSize(200,40)
 .setFont(createFont("Chalkboard",20))
 .setColorBackground(color(67,41,24))
 .setAutoClear(false)
 .setCaptionLabel("")

 ;

  cp5.addButton("START")
   //.setValue(128)
  .setPosition(225,720)
   //.setImages(imgs)
  .setFont(createFont("Chalkboard",40))
  .setSize(200,100)
   //.updateSize()
   ;

  cp5.addButton("RESET")
   //.setValue(128)
  .setPosition(475,720)
   //.setImages(imgs)
   .setFont(createFont("Chalkboard",40))
  .setSize(200,100)
  
  .setColorBackground(color(255,0,0))
  .setColorForeground(color(255,100,100))
  
   //.updateSize()
   ;
   


  
  
}

public void GUI_draw() {
  

    

  background(249,184,117);
  //fill(227, 230, 255);
  
  
  strokeWeight(5);
  
  fill(bg);
  rect(100,250,300,400);
  
  stroke(40,150,20);
  
  fill(58,170,53);
  rect(100,550,300,100);
  
  stroke(67,41,24);
  noFill();
    rect(100,250,300,400);
  
  
  
  rectMode(CENTER);
  
  imageMode(CORNER);
  
  if (hairmode==2){tint(r,g,b);image(medium_img,150,300,210,280);}
  else if (hairmode==3){tint(r,g,b); image(long_img,150,300,210,280);}
  
  tint(255-skin, 230-skin,210-skin);
  image(skin_img,150,300,210,280);
  
  tint(r,g,b);
  image(hair_img,150,300,210,280);
  
  noTint();
  
  image(cloth_img,150,300,210,280);
  image(hat_img,150,300,210,280);
  image(eye_img,150,300,210,280);
  
  
  
  rectMode(CORNER);
  noStroke();

  fill(67,41,24);
  
  textSize(50);
  text("CREATE YOUR CHARACTER!", 150,180);
  textSize(32);
  
  text("FLOWER GARDEN", 150,100);
  text("NAME", 480,200+32+70);
  text("HAIR", 480, 392);
  text("SKIN", 480, 592);
  
  PImage rose = loadImage("rose4.png");
  
  image(rose,110,500,100,100);
  
  PImage tulip = loadImage("tulip3.png");
  
  image(tulip,300,500,100,100);
  
  textSize(20);
  text("by Sehee Lee",750,880);
  
  
  user.set_hair(r,g,b);
  user.set_skin(255-skin, 230-skin,210-skin);
  user.set_hairmode(hairmode);

}

public void controlEvent(ControlEvent theEvent) {

  if(theEvent.isFrom(r1)) {
  //print("got an event from "+theEvent.getName()+"\t");
  for(int i=0;i<theEvent.getGroup().getArrayValue().length;i++) {
    //print(int(theEvent.getGroup().getArrayValue()[i]));
  }
  //println("\t "+theEvent.getValue());
  //hairmode=1;
  }
  
  else if(theEvent.getController().getName()=="START")
  {
    user.set_name(cp5.get(Textfield.class,"NAME").getText());
    
    user.set_hair(r,g,b);
    user.set_skin(255-skin,230-skin,210-skin);
    
    gui = false;
  }
  //n = 0;
  else if(theEvent.getController().getName()=="RESET")
  {
    
    cp5.get(Slider.class,"r").setValue(100);
    cp5.get(Slider.class,"g").setValue(100);
    cp5.get(Slider.class,"b").setValue(100);
    cp5.get(Slider.class,"skin").setValue(100);
    hairmode = 1;
    clear();
  }
}

void radioButton(int a) {
  hairmode = a;

}

public void clear() {
  cp5.get(Textfield.class,"NAME").clear();
}

void slider(float theColor) {
  myColor = color(theColor);
  //println("a slider event. setting background to "+theColor);
}
