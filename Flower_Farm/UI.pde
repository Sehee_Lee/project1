

public class UI
{
  
  //UI
  private PImage tool1 = loadImage("tool1.png");
  private PImage tool2 = loadImage("tool2.png");
  private PImage tool3 = loadImage("tool3.png");
  
  UI()
  {
  
  }
  
  public void draw(int tool_x, int tool_y, User user)
  {
    
    stroke(150);
    strokeWeight(5);
    fill(255);
    
    rect(tool_x,0,200,100);
    textSize(32);
    fill(0);
    
    Chalkboard = createFont("Chalkboard",32);
    textFont(Chalkboard);
    text(user.getName(),10,40);
    text("Day " + user.getDate(),10,80);

    
    fill(230);

    rect(tool_x,tool_y,100,100);
    
    rect(tool_x,tool_y+100,100,100);
    rect(tool_x,tool_y+200,100,100);
    
    if (user.getState()==1){fill(255);stroke(200,0,0);strokeWeight(5);rect(tool_x,tool_y,100,100);}
    else if (user.getState()==2){fill(255);stroke(200,0,0);strokeWeight(5);rect(tool_x,tool_y+100,100,100);}
    else if (user.getState()==3){fill(255);stroke(200,0,0);strokeWeight(5);rect(tool_x,tool_y+200,100,100);}

    image(tool1, tool_x, tool_y, 100,100);
    image(tool2, tool_x, tool_y+100, 100,100);
    image(tool3, tool_x, tool_y+200, 100,100);
    
    
    int f_r = FC.getState().row();
    int f_c = FC.getState().col();
    
    PImage seed_image = loadImage(f_r +""+f_c+".png");
    
    image(seed_image, tool_x+25, tool_y+240,40,40);

    textSize(15);
    fill(0);
    text("1" , tool_x+5,tool_y + 15);
    
    text("2", tool_x+5, tool_y+115);
    text("3", tool_x+5, tool_y+215);
  }
  
  
}
