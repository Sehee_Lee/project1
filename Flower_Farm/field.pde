


public class Field
{
 
  public static final int FIELD_SIZE = 7;
  
  private Section [][] field;
  //private RowCol startLocation;
  //private ArrayList<RowCol> selection;
  private float fSize;
  private float sSize;
  private PImage scrt_img = loadImage("scrt.png");
  private PImage seed_img = loadImage("seed.png");
  private PImage plant_img = loadImage("plant.png");
  private PImage plant2_img = loadImage("plant2.png");
  
  //farm field
  
  public Field (int size)
  {
    field = new Section [FIELD_SIZE][FIELD_SIZE];
    fSize = size;
    sSize = size/field.length;
    
    for (int row=0; row<field.length; row++)
      for (int col=0; col<field.length; col++)
      {
        field[row][col] = new Section(rowColToCoord(row,col), sSize);
      }
    
  }
  
  
  public void draw()
  {
    
    //background
    for (int row=0; row<field.length; row++)
      for (int col=0; col<field.length; col++)
      {
        stroke(40,150,20);
        strokeWeight(5);
        fill(58,170,53);
        //fill(202,158,103);
        rect(row*sSize, col*sSize, sSize, sSize);

      }
    //sections
    for (int row=0;row<field.length;row++)
      for (int col=0; col<field.length; col++)
      {
        //field[row][col].draw();

        if (field[row][col].getWatered() == true)
        {
          fill(177,127,74);
          rect(row*sSize, col*sSize, sSize, sSize); 
        }

        if(field[row][col].getScrted()==true)
        {
          stroke(147,96,55);
          
          if (field[row][col].getWatered() == true) {fill(177,127,74);}
          else {fill(202,158,103);}
          rect(row*sSize, col*sSize, sSize, sSize);
          imageMode(CORNER);
          image(scrt_img, row*sSize,col*sSize, sSize, sSize);
        }

        
        if(field[row][col].getSeeded() == true)
        {
          imageMode(CORNER);
          
          if (field[row][col].getDate() == 0)
          {image(seed_img, row*sSize,col*sSize, sSize, sSize);}
          else if (field[row][col].getDate() == 1 )
          {
            image(plant_img, row*sSize,col*sSize, sSize, sSize);
          }
          else if ( field[row][col].getDate() == 2)
          {
            image(plant2_img, row*sSize, col*sSize, sSize, sSize);
          }
          
          else if (field[row][col].getDate() > 2)
          {
            PImage flower = loadImage(field[row][col].getFlower()+".png");
            image(flower, row*sSize, col*sSize-5, sSize, sSize);
            field[row][col].setBloom(true);
          }
          
        }
        
      }
    
  }
  
  
  
  public void set_section_date(int row, int col)
  {
    field[row][col].setDate();
  }
  
  
  public void set_section_date(RowCol rc, int date)
  {
    field[rc.row()][rc.col()].setDate(date);
  }
  
  public Section getSection (RowCol rc)
  {
    if (!inBouonds(rc)) return null;
    return field[rc.row()][rc.col()];
  }
  
  public Section getSection (int r, int c)
  {
    if (!inBouonds(r,c)) return null;
    return field[r][c];
  }
  
  private boolean inBouonds (RowCol rc)
  {
    if (rc==null) return false;
    return inBouonds (rc.row(), rc.col());
  }

  private boolean inBouonds (int row, int col)
  {
    if (row <0 || row >= field.length) return false;
    if (col <0 || col >= field.length) return false;
    return true;
  }

  
  public void set_section_watered(RowCol rc, boolean b)
  {
    field[rc.row()][rc.col()].setWatered(b);
  }
  
  public void set_section_watered(int row, int col, boolean b)
  {
    field[row][col].setWatered(b);
  }
  
  
  public void set_section_scrted(RowCol rc, boolean b)
  {
    field[rc.row()][rc.col()].setScrted(b);
  }  
  
  public void set_section_seeded(RowCol rc, boolean b)
  {
    field[rc.row()][rc.col()].setSeeded(b);
  }
  
  public void set_section_bloom(RowCol rc, boolean b)
  {
    
     field[rc.row()][rc.col()].setBloom(b);
  }
  
  public void set_flower(RowCol rc, String s)
  {
    field[rc.row()][rc.col()].setFlower(s);
    
  }
  
  public void set_flower_int(RowCol rc, int c, int kind)
  {
     field[rc.row()][rc.col()].setFlowerInt(c,kind);
  }
 
  private RowCol coordToRowCol (int x, int y)
  {
    if (x<0 || x>fSize || y<0 || y>fSize) return null;  
    return new RowCol (int(x/sSize), int(y/sSize));
  }
  
  //private PVector rowColToCoord (RowCol rc)
  //{
  //  return rowColToCoord (rc.row(), rc.col());
  //}
  
  private PVector rowColToCoord (int row, int col)
  {
    //if (!inBouonds(row, col)) return null;
    return new PVector (sSize/2+col*sSize, sSize/2+row*sSize);
  }
  
  private int calculate_breeding(int a, int b)
  {
    if (a==0 && b==1){return 4;}
    else if (a==1 && b==0){return 4;}//red + yellow = orange
    else if (a==0 && b==2){return 3;}
    else if (a==2 && b==0){return 3;}//red + white = pink
    else if (a==2 && b==2) {return 5;}//white + white = blue
    else if (a==0 && b == 5){return 6;}
    else if (a==5 && b == 0){return 6;} //red + blue = purple
    else if (a==7 && b == 7){return 7;} // purple + purple = black
    else{return b;}
  }
  
  
  
  public void crossbreeding()
  {
    for (int i = 1; i<field.length-1;i++)//except corner
    for (int j = 1; j<field.length-1; j++)//except corner
    {
      if (field[i][j].getKind()==field[i+1][j].getKind() && field[i][j].getBloom()==true && field[i+1][j].getBloom()==true) // if right side of section has same flower and bloomed
      {
        int a = field[i][j].getC();
        int b = field[i+1][j].getC();
        int kind = field[i][j].getKind();

        for (int k = i; k<i+2;k++)
        {
          if (field[k][j-1].getScrted()==true && field[k][j-1].getSeeded()==false)
          {
            //make a new flower!
            
            float p = random(0,50);
            
            if (p>25){
            field[k][j-1].setFlower(new Flower(calculate_breeding(a,b),kind).get_name());
            field[k][j-1].setFlowerInt(calculate_breeding(a,b),kind);
            field[k][j-1].setDate(1);
            field[k][j-1].setSeeded(true);
            FC.setPossible(calculate_breeding(a,b), kind, true);
            }
          }

        }
        
        for (int k = i; k<i+2;k++)
          if(field[k][j+1].getScrted()==true && field[k][j+1].getSeeded()==false)
          {      
            float p = random(0,50);
            
            if (p>25){
            field[k][j+1].setFlower(new Flower(calculate_breeding(a,b),kind).get_name());
            field[k][j+1].setFlowerInt(calculate_breeding(a,b),kind);
            field[k][j+1].setDate(1);
            field[k][j+1].setSeeded(true);
            FC.setPossible(calculate_breeding(a,b), kind, true);
            }
            
          }
        
      }
      else if (field[i][j].getKind() == field[i][j+1].getKind() && field[i][j].getBloom()==true && field[i][j+1].getBloom()==true) // if the down side of section has same flower and bloomed
      {      
        int a = field[i][j].getC();
        int b = field[i][j+1].getC();
        int kind = field[i][j].getKind();
        
        for (int k = j; k<j+2;k++)
        {
          if (field[i-1][k].getScrted()==true && field[i-1][k].getSeeded()==false)
          {
            //make a new flower!
            
            float p = random(0,50);
            
            if (p>25){
            field[i-1][k].setFlower(new Flower(calculate_breeding(a,b),kind).get_name());
            field[i-1][k].setFlowerInt(calculate_breeding(a,b),kind);
            field[i-1][k].setDate(1);
            field[i-1][k].setSeeded(true);
            FC.setPossible(calculate_breeding(a,b), kind, true);
            }
          }

        }
        
        for (int k = j; k<j+2;k++)
          if(field[i+1][k].getScrted()==true && field[i+1][k].getSeeded()==false)
          {      
            float p = random(0,50);
            
            if (p>25){
            field[i+1][k].setFlower(new Flower(calculate_breeding(a,b),kind).get_name());
            field[i+1][k].setFlowerInt(calculate_breeding(a,b),kind);
            field[i+1][k].setDate(1);
            field[i+1][k].setSeeded(true);
            FC.setPossible(calculate_breeding(a,b), kind, true);
            }
            
          }
      }

    }
    
    for (int i = 0; i < 6;i++)//upper corner;
    {
      
      int a = field[i][0].getC();
      int b = field[i+1][0].getC();
      int kind = field[i][0].getKind();
        
      if (field[i][0].getKind()==field[i+1][0].getKind() && field[i][0].getBloom()==true && field[i+1][0].getBloom()==true)
      {
        for (int k = i; k<i+2;k++)
        {
          if (field[k][1].getScrted()==true && field[k][1].getSeeded()==false)
          {
            //make a new flower!
            
            float p = random(0,50);
            
            if (p>25){
            field[k][1].setFlower(new Flower(calculate_breeding(a,b),kind).get_name());
            field[k][1].setFlowerInt(calculate_breeding(a,b),kind);
            field[k][1].setDate(1);
            field[k][1].setSeeded(true);
            FC.setPossible(calculate_breeding(a,b), kind, true);
            }
          }
        }

      }
    }
      
      
    for (int i = 0; i < 6;i++)//down corner;
    {
      
      int a = field[i][6].getC();
      int b = field[i+1][6].getC();
      int kind = field[i][6].getKind();
        
      if (field[i][6].getKind()==field[i+1][6].getKind() && field[i][6].getBloom()==true && field[i+1][6].getBloom()==true)
      {
        for (int k = i; k<i+2;k++)
        {
          if (field[k][5].getScrted()==true && field[k][5].getSeeded()==false)
          {
            //make a new flower!
            
            float p = random(0,50);
            
            if (p>25){
            field[k][5].setFlower(new Flower(calculate_breeding(a,b),kind).get_name());
            field[k][5].setFlowerInt(calculate_breeding(a,b),kind);
            field[k][5].setDate(1);
            field[k][5].setSeeded(true);
            FC.setPossible(calculate_breeding(a,b), kind, true);
            }
          }

        }

      }
      
      
    }
    
    
    for (int j = 0; j < 6;j++)//leftcorner;
    {
      
      int a = field[0][j].getC();
      int b = field[0][j+1].getC();
      int kind = field[0][j].getKind();
        
      if (field[0][j].getKind()==field[0][j+1].getKind() && field[0][j].getBloom()==true && field[0][j+1].getBloom()==true)
      {
        for (int k = j; k<j+2;k++)
        {
          if (field[1][j].getScrted()==true && field[1][j].getSeeded()==false)
          {
            //make a new flower!
            
            float p = random(0,50);
            
            if (p>25){
            field[1][j].setFlower(new Flower(calculate_breeding(a,b),kind).get_name());
            field[1][j].setFlowerInt(calculate_breeding(a,b),kind);
            field[1][j].setDate(1);
            field[1][j].setSeeded(true);
            FC.setPossible(calculate_breeding(a,b), kind, true);
            }
          }

        }

      }

    }
    for (int j = 0; j < 6;j++)//right corner;
    {
      
      int a = field[6][j].getC();
      int b = field[6][j+1].getC();
      int kind = field[6][j].getKind();
        
      if (field[6][j].getKind()==field[6][j+1].getKind() && field[6][j].getBloom()==true && field[6][j+1].getBloom()==true)
      {
        for (int k = j; k<j+2;k++)
        {
          if (field[5][j].getScrted()==true && field[5][j].getSeeded()==false)
          {
            //make a new flower!
            
            float p = random(0,50);
            
            if (p>25){
            field[5][j].setFlower(new Flower(calculate_breeding(a,b),kind).get_name());
            field[5][j].setFlowerInt(calculate_breeding(a,b),kind);
            field[5][j].setDate(1);
            field[5][j].setSeeded(true);
            FC.setPossible(calculate_breeding(a,b), kind, true);
            }
          }

        }

      }
    }

    
  }
  
}
