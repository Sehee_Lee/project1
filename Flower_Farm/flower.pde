public class FlowerCollection
{
  private Flower[][] collection;
  
  
  private PImage collectImage = loadImage("collection.png");
  private PImage noflower = loadImage("noflower.png");
  int x;
  int y;
  
  private RowCol state;
  
  
  FlowerCollection()
  {
  collection = new Flower[8][3];
    
  for (int i = 0; i < 8 ;i++ )
    for (int j = 0; j < 3 ; j++)
    {
     collection[i][j] = new Flower(i,j);
    }
    
    state = new RowCol(0,0);
  }
  
  public void draw(int xLoc, int yLoc)
  {
    x = xLoc;
    y = yLoc;
    
    pushMatrix();
    translate(xLoc,yLoc);
    image(collectImage,0,0,640,240);
    
    
    for (int i = 0; i < 8 ;i++ )
      for (int j = 0; j < 3 ; j++)
      {
       if(collection[i][j].getPossible()==false){image(noflower,80*i,80*j,80,80);}
       
      }
      
    noFill();stroke(200,0,0);strokeWeight(5);
    rect(80*state.row(),80*state.col(),80,80);
    
    
    popMatrix();
    
    //rectMode(CORNER);
    
    //fill(255);
    //noStroke();
    //rect(xLoc,yLoc-50,640,50);
    
    //rectMode(CORNER);
    int c = 0;
    for(int i = 0; i<8; i++)
    for (int j = 0; j<3;j++)
    {
      if (collection[i][j].getPossible()==true){c++;}
    }
    
    textSize(32);
    fill(255);
    
    text("collection completed "+ c*100/24 + " %",xLoc+100,yLoc-10);
  
    

  }
  
  public int getX(){return x;}
  public int getY(){return y;}
  
  public void setState(RowCol mouseXY){state=mouseXY;}
  public RowCol getState(){return state;}
  
  public Flower getFlower(int row, int col){return collection[row][col];}
  
  public void setPossible(int c, int kind, boolean b){collection[c][kind].setPossible(b);}

  
   private RowCol coordToRowCol (float x, float y)
  {
    if (x<0 || x>640 || y<0 || y>240) return null;  
    return new RowCol (int(x/80), int(y/80));
  }

  
}



public class Flower
{
  int c;
  int kind;
  String name="";
  boolean possible;
  
  Flower(int c, int kind)
  {
  
  this.c = c;
  this.kind = kind;
  
  String f = "";
  
  if (kind == 0){f = "tulip";}
  else if (kind == 1){f = "rose";}
  else if (kind == 2){f = "cosmos";}
  
  name = f + c;
  
  if (c<3){possible = true;}
  else {possible = false;}
  
  }
  
  public boolean getPossible(){return possible;}
  
  public void setPossible(boolean b){possible = b;}
  
  
  public String get_name(){return name;}
  public int get_kind(){return kind;}
  public int get_color(){return c;}
  
  
}
